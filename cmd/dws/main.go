/**
 * File: main.go
 * Written by:  Stephen M. Reaves
 * Created on:  Wed, 08 Jun 2022
 * Description: Generates default web server structure
 */

package main

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/reavessm/dws/pkg/config"
	"gitlab.com/reavessm/dws/pkg/template"
)

func main() {
	if err := config.ParseFlags(); err != nil {
		fmt.Printf("\n%v\n", err)
		os.Exit(1)
	}

	if err := run(); err != nil {
		log.Println(err)
		os.Exit(2)
	}
}

func run() error {
	cfg := config.New()

	for _, fn := range template.GetGenerators() {
		if err := fn(cfg); err != nil {
			return err
		}
	}

	return nil
}
