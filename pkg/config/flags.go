package config

import (
	"errors"
	"flag"
	"time"
)

func ParseFlags() error {
	binaryName := flag.String("n", "", "Name of binary")
	moduleName := flag.String("m", DefaultModule, "Name of module")
	rootPath := flag.String("r", DefaultRoot, "Root of generated file tree")
	localDevFlag := flag.Bool("l", false, "Should 'go mod' redirect to 'pwd'?")

	flag.Parse()

	if *binaryName != "" {
		Name = *binaryName
	} else {
		flag.Usage()
		return errors.New("Please enter a name")
	}

	if *moduleName != "" {
		Module = *moduleName
	}

	// RFC1123 minus time(zone)
	Date = time.Now().Format("Mon, 02 Jan 2006")

	if *rootPath != "" {
		Root = *rootPath
	}

	if *localDevFlag {
		LocalDev = *localDevFlag
	}

	return nil
}
