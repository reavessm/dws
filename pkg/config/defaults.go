package config

const (
	DefaultModule string = "gitlab.com/user/pkgName"
	DefaultRoot   string = "./tmp"
)

var (
	Name     string
	Module   string
	Date     string
	Root     string
	LocalDev bool
)

type Model struct {
	Name     string
	Module   string
	Date     string
	Root     string
	LocalDev bool
}

func New() *Model {
	return &Model{
		Name:     Name,
		Module:   Module,
		Date:     Date,
		Root:     Root,
		LocalDev: LocalDev,
	}
}
