package template

// TODO: Break out 'main' files into separate binaries and start them

// cmd
const (
	Main string = `/**
 * File: maing.go
 * Written by:  <++>
 * Created on:  {{.Date}}
 * Description: <++>
 */

package main

import (
	"fmt"
	"log"
	"os"

	"{{.Module}}/pkg/config"
)

func main() {
	if err := config.ParseFlags(); err != nil {
		fmt.Printf("\n%v\n", err)
		os.Exit(1)
	}

	if err := run(); err != nil {
		log.Println(err)
		os.Exit(2)
	}
}

func run() error {
	return nil
}
`
)

// pkg/config
const (
	Config_Flags string = `package config

import (
	"errors"
	"flag"
)

func ParseFlags() error {
	addressFlag := flag.String("address", DefaultAddress, "Network address to listen on")
	apiPortFlag := flag.String("api-port", DefaultApiPort, "Network port for API server")
	uiPortFlag := flag.String("ui-port", DefaultUiPort, "Network port for web ui")
	barFlag := flag.String("bar", "", "Bar to baz")

	flag.Parse()

	if *addressFlag != "" {
		Address = *addressFlag
	}

	if *apiPortFlag != "" {
		ApiPort = *apiPortFlag
	}

	if *uiPortFlag != "" {
		UiPort = *uiPortFlag
	}

	if *barFlag != "" {
		Bar = *barFlag
	} else {
		flag.Usage()
		return errors.New("Please enter a bar")
	}

	return nil
}
`

	Config_Defaults string = `package config

const (
	DefaultAddress string = "localhost"
	DefaultApiPort string = "8081"
	DefaultUiPort string = "8080"
)

var (
	Address string
	ApiPort string
	UiPort string
	Bar string
)
`
)

// pkg/handlers/rest.go
const (
	Handlers_REST string = `package handlers

import (
	"net/http"
	"encoding/json"
)

type RestHandler interface {
	List(w http.ResponseWriter, r *http.Request)
	Get(w http.ResponseWriter, r *http.Request)
	Create(w http.ResponseWriter, r *http.Request)
	Patch(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

func notImplemented(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotImplemented)
	response, _ := json.Marshal(` + "`" + `{"error": "not implemented"}` + "`" + `)
	_, _ = w.Write(response)
}
`
)

// pkg/handlers/foo.go
const (
	Handers_Foo string = `package handlers

import (
	"net/http"
)

var _ RestHandler = fooHandler{}

type fooHandler struct {}

func NewFooHandler() *fooHandler {
	return &fooHandler{}
}

func (h fooHandler) List(w http.ResponseWriter, r *http.Request) {
	notImplemented(w)
}

func (h fooHandler) Get(w http.ResponseWriter, r *http.Request) {
	notImplemented(w)
}

func (h fooHandler) Create(w http.ResponseWriter, r *http.Request) {
	notImplemented(w)
}

func (h fooHandler) Patch(w http.ResponseWriter, r *http.Request) {
	notImplemented(w)
}

func (h fooHandler) Delete(w http.ResponseWriter, r *http.Request) {
	notImplemented(w)
}
`
)

// pkg/servers/interface.go
const (
	Servers_Interface string = `package servers

import (
	"net"
)

type Server interface {
	Start()
	Stop() error
	Listen() (net.Listener, error)
	Serve(net.Listener)
}
`
)

// pkg/servers/ui.go
const (
	Servers_UI string = `package servers

import (
	"context"
	"log"
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"{{.Module}}/pkg/config"
)

type uiServer struct {
	httpServer *http.Server
}

var _ Server = &uiServer{}

func NewUIServer() Server {
	srv := &uiServer{}

	mainRouter := mux.NewRouter()

	httpHandler := handlers.NewHttpHandler()

	mainRouter.HandleFunc("", httpHandler.Main).Methods(http.MethodGet)

	var mainHandler http.Handler = mainRouter

	srv.httpServer = &http.Server {
		Handler: mainHandler,
		Addr: fmt.Sprintf("%s:%s", config.Address, config.UiPort),
	}

	return srv
}

func (s *uiServer) Start() {
	listener, err := s.Listen()
	if err != nil {
		log.Fatalln("Unable to start API server: %v", err)
	}
	s.Serve(listener)
}

func (s *uiServer) Stop() error {
	return s.httpServer.Shutdown(context.Background())
}

func (s *uiServer) Listen() (net.Listener, error) {
	return net.Listen("tcp", fmt.Sprintf("%s:%s", config.Address, config.UiPort))
}

func (s *uiServer) Serve(l net.Listener) {
	if err := s.httpServer.Serve(l); err != nil {
		log.Fatalln(err)
	}
}
`
)

// pkg/servers/api.go
const (
	Servers_API string = `package servers

import (
	"context"
	"log"
	"fmt"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	"{{.Module}}/pkg/config"
)

type apiServer struct {
	httpServer *http.Server
}

var _ Server = &apiServer{}

func NewAPIServer() Server {
	srv := &apiServer{}

	mainRouter := mux.NewRouter()

	fooHandler := handlers.NewFooHandler()

	apiRouter := mainRouter.PathPrefix("/api/{{.Name}}/v1/foo").Subrouter()
	apiRouter.HandleFunc("", fooHandler.List).Methods(http.MethodGet)

	var mainHandler http.Handler = mainRouter

	srv.httpServer = &http.Server{
		Handler: mainHandler,
		Addr: fmt.Sprintf("%s:%s", config.Address, config.ApiPort),
	}

	return srv
}

func (s *apiServer) Start() {
	listener, err := s.Listen()
	if err != nil {
		log.Fatalln("Unable to start API server: %v", err)
	}
	s.Serve(listener)
}

func (s *apiServer) Stop() error {
	return s.httpServer.Shutdown(context.Background())
}

func (s *apiServer) Listen() (net.Listener, error) {
	return net.Listen("tcp", fmt.Sprintf("%s:%s", config.Address, config.ApiPort))
}

func (s *apiServer) Serve(l net.Listener) {
	if err := s.httpServer.Serve(l); err != nil {
		log.Fatalln(err)
	}
}
`
)

// static
const (
	Static_HTML string = `<!DOCTYPE html>
<html>
  <head>
    <body>
      <p>Hello World!</p>
    </body>
  </head>
</html>
`
	Static_CSS string = ``

	Static_JS string = ``
)

// go.mod
const (
	Go_Mod string = `module {{.Module}}

go 1.16
{{if .LocalDev}}
replace {{.Module}} => ./
{{end}}
require github.com/gorilla/mux v1.8.0
`
)
