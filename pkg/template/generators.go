package template

import (
	"fmt"
	"os"
	"text/template"

	"gitlab.com/reavessm/dws/pkg/config"
)

type Generator func(c *config.Model) error

func GetGenerators() []Generator {
	return []Generator{
		GenerateMain,
		GenerateGoMod,
		GenerateConfigDefaults,
		GenerateConfigFlags,
		GenerateServersInterface,
		GenerateServersUI,
		GenerateServersAPI,
		GenerateHandlersREST,
		GenerateHandlersFoo,
	}
}

func GenerateMain(c *config.Model) error {
	return generate("Main", Main, fmt.Sprintf("cmd/%s", c.Name), "main.go", c)
}

func GenerateConfigDefaults(c *config.Model) error {
	return generate("Config_Defaults", Config_Defaults, "pkg/config", "defaults.go", c)
}

func GenerateConfigFlags(c *config.Model) error {
	return generate("Config_Flags", Config_Flags, "pkg/config", "flags.go", c)
}

func GenerateServersUI(c *config.Model) error {
	return generate("Servers_UI", Servers_UI, "pkg/servers", "ui.go", c)
}

func GenerateServersAPI(c *config.Model) error {
	return generate("Servers_API", Servers_API, "pkg/servers", "api.go", c)
}

func GenerateServersInterface(c *config.Model) error {
	return generate("Servers_Interface", Servers_Interface, "pkg/servers", "interface.go", c)
}

func GenerateGoMod(c *config.Model) error {
	return generate("Go_Mod", Go_Mod, ".", "go.mod", c)
}

func GenerateHandlersREST(c *config.Model) error {
	return generate("Handlers_REST", Handlers_REST, "pkg/handlers", "rest.go", c)
}

func GenerateHandlersFoo(c *config.Model) error {
	return generate("Handlers_Foo", Handers_Foo, "pkg/handlers", "foo.go", c)
}

func generate(tplName, tpl, dirPath, fileName string, c *config.Model) error {
	t := template.New(tplName)

	t.Parse(tpl)

	dir := fmt.Sprintf("%s/%s", c.Root, dirPath)

	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}

	file, fErr := os.Create(fmt.Sprintf("%s/%s", dir, fileName))
	if fErr != nil {
		return fErr
	}

	return t.Execute(file, c)
}
