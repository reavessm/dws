.DEFAULT:
	build

build_flags=-a -ldflags '-extldflags "-static"'

build:
	go build $(build_flags) -o dws cmd/dws/main.go

.PHONY: build

clean:
	rm -f dws

.PHONY: clean

test: build
	./dws -n foo -m gitlab.reaves.dev/reavessm/dws -l

.PHONY: test
